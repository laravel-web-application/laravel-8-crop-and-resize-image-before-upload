<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

### Things to do list:

1. Clone this
   repository: `git clone https://gitlab.com/laravel-web-application/laravel-8-crop-and-resize-image-before-upload.git`
2. Go inside the folder: `cd laravel-8-crop-and-resize-image-before-upload`
3. Run `composer install`
4. Run `php artisan key:generate`
5. Run `php artisan serve`
6. Open your favorite browser: http://localhost:8000/image-cropper

### Image Screen shot

Upload & Crop Image

![Upload & Crop Image](img/crop.png "Upload & Crop Image")

Reference: https://w3alert.com/blog/laravel-8-crop-and-resize-image-before-upload-tutorial
